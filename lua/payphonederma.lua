PANEL = {} 
--Основная дерма таксы с колесом и картиночками
--При нажатии на невидимую кнопку над колесом создает PayphoneDermaPopup
function PANEL:Init()
	self.taxamat = Material('taxa.png')
	self.kolesomat = Material('koleso.png')
	self.pmat = Material("zheleznaya_pizdyuchka.png")
	self:SetTitle("Таксофон")
	self:SetSize(600,600)
	self:SetSizable(false)
	self:SetDeleteOnClose(true)
	self:Center()
	self.btnMaxim:SetVisible(false)
    self.btnMinim:SetVisible(false)
    self.btnClose:SetVisible(false)

    self.angle = 0
    self.rotdir = -8 
    self.isrotating = false
    self.OldPaint = self.Paint
    self:SetBackgroundBlur(true)

	self.mainBtn = vgui.Create("DButton",self)
	self.mainBtn:SetPos(127,52)
	self.mainBtn:SetSize(344,338)
	self.mainBtn:SetText("")
	self.mainBtn.DoClick = function()
		--popup:AddBtn(текст кнопки, ключ в таблице с данными для посроения формы выхова)
		if self.isrotating then return end
		self.isrotating = true
		net.Start("payphoneComm")
		net.WriteString("req_sound")
		net.WriteEntity(self.ent)
		net.SendToServer()
		
	end


	self.Paint = function(pnl,w,h)
		if self.isrotating then
			self.angle = self.angle + self.rotdir
			if self.angle < -270 then 
				self.rotdir = 4 
			
			end
			if self.angle > 0 then 
				self.rotdir = -8 self.angle = 0 self.isrotating = false 
				self.popup = vgui.Create("PayphoneDermaPopup",self)
				--print(self.popup)
				self.popup:AddSpacer("Экстренные вызовы")
				self.popup:AddSpacer("Платные услуги")
				self.popup:AddBtn("01","fire")
				self.popup:AddBtn("Диетолог","diet")
				self.popup:AddBtn("02","police")
				self.popup:AddBtn("Такси","taxi")
				self.popup:AddBtn("03","med")
				self.popup:AddBtn("Повар","cook")
				self.popup:AddBtn("04","gas")
				self.popup:AddBtn("Механик","mech")
				self.popup:AddCloseBtn()
			end
		end
		self:OldPaint(0,0)
		surface.SetMaterial(pnl.taxamat)
		surface.SetDrawColor(255,255,255)
		surface.DrawTexturedRect(0,0,w,h)
		surface.SetMaterial(pnl.kolesomat)
		surface.DrawTexturedRectRotated(299,221,344,338,self.angle)
		surface.SetMaterial(pnl.pmat)
		surface.DrawTexturedRect(0,0,w,h)

	end

	self.mainBtn.Paint = function(pnl,w,h)
		draw.RoundedBox(0,0,0,w,h,Color(255,255,255,0))
	end



	--проверки рангов дял выполнения этих действи дублируются на сервере 
	if LocalPlayer():GetUserGroup() == "creator" or LocalPlayer():GetUserGroup() == "developer" or LocalPlayer():GetUserGroup() == "manager" then
		self.saveBtn = vgui.Create("DButton",self)
		self.saveBtn:SetText("Save to DB")
		self.saveBtn:SetSize(100,20)
		self.saveBtn.DoClick = function()
			net.Start("payphoneComm")
			net.WriteString("save")
			net.WriteEntity(self.ent)
			net.WriteString(self.addr:GetText() or "")
			net.SendToServer()
		end
		self.addr = vgui.Create("DTextEntry",self)
		self.addr:SetPos(0,25)
		self.addr:SetSize(100,20)
		timer.Simple(0.1,function()--Богомерзкий костыль
		self.addr:SetText(self.ent:GetAddress())
		end)
		self.remBtn = vgui.Create("DButton",self)
		self.remBtn:SetText("Remove")
		self.remBtn:SetPos(0,50)
		self.remBtn:SetSize(100,20)
		self.remBtn.DoClick = function()
			net.Start("payphoneComm")
			net.WriteString("remove")
			net.WriteEntity(self.ent)
			net.SendToServer()
		end


	end
	self:MakePopup()
end


vgui.Register("PayphoneDerma",PANEL,"DFrame")