include("payphonederma.lua")
include("payphonedermapopup.lua")
--include("calldialog.lua")
local minHitDistanceSqr = 150*150

hudMarks = {}
markerMat = Material( "materials/Waypoint.png" )


--Место приема всех сетевых сообщений от сервера
net.Receive("payphoneComm",function (len,ply) 
	local command = net.ReadString()
	if command == "open_menu" then
		local panel = vgui.Create("PayphoneDerma")
		local ent = net.ReadEntity()
		panel.ent = ent

	elseif command == "delete_mark" then
		local id = net.ReadInt(32)
		for k,v in pairs(hudMarks) do
			if v.id == id then
				hudMarks[k] = nil
				break
			end
		end
	elseif command == "fire_mark" or command == "police_mark" then
		local markData = net.ReadTable()
		--таблица состоит из: phone,desc,name,time
		surface.PlaySound( "common/warning.wav" )

		markData.title = ((command == "fire_mark") and "Сообщение о пожаре!") or "Поступил вызов!"
		markData.pos = markData.phone:GetPos()
		markData.pos.z = markData.pos.z+50
		markData.addr = markData.phone:GetAddress()
		notification.AddLegacy( "Поступил вызов в районе "..markData.addr, NOTIFY_GENERIC, 2 )
		markData.onply = false


		hudMarks[markData.time] = markData
		--PrintTable(hudMarks)
		timer.Simple(300-(CurTime()-markData.time),function() hudMarks[markData.time] = nil end)

	elseif command == "offerBounty"	then
		local offerData = net.ReadTable()
		--PrintTable(offerData)
		local popup = vgui.Create("PayphoneBountyOfferPopup")
		popup.id = offerData.id
		popup:SetText(offerData.caller:Nick().." вызывает вас на "..offerData.street..". Вам нужно успеть на вызов вовремя, чтобы получить 300 рублей")
	
	elseif command == "general_mark" then
		local markData = net.ReadTable()
		markData.title = "Вызов!"
		markData.pos.z = markData.pos.z+50
		markData.addr = markData.street
		markData.time = markData.startTime
		markData.ply = markData.caller
		markData.onply = false
		hudMarks[markData.time] = markData

		timer.Simple(300-(CurTime()-markData.time),function() hudMarks[markData.time] = nil end)

	elseif command == "offerConfirm" then
		--print("CONFIRM HERE")
		local id = net.ReadInt(32)
		local pnl = Derma_Query("Подтверждаете ли вы прибытие рабочего?","Подтверждение","Да",function(self) 
				net.Start("payphoneComm")
				net.WriteString("acceptConfirm")
				net.WriteInt(id,32)
				net.SendToServer()
			end
			,"Нет",function(self)
				self:GetParent():Close()
			end)
	elseif command == "delall" then
		hudMarks = {}
	end



end)
local stupidFrameCounter = 0
function drawmarkers()
	stupidFrameCounter = stupidFrameCounter + 1
	if stupidFrameCounter > 30 then 
		stupidFrameCounter = 0 
		for i,mark in pairs(hudMarks) do
			if (not mark.onply) and LocalPlayer():GetPos():DistToSqr(mark.pos) < 30000 then
				if (mark.t == "pay" or mark.t == "police") then
					hudMarks[i].onply = true
				else
					hudMarks[i] = nil
				end
			end
		end
	end
	--PrintTable(hudMarks)
	for i,mark in pairs(hudMarks) do
			--print("mark")
		local pos = nil
		if mark.onply then
			if ! IsValid(mark.ply) then continue end
			pos = mark.ply:EyePos()
			pos.z = pos.z + 40
			pos = pos:ToScreen()
		else
			pos = mark.pos:ToScreen()
		end
		if pos then
			surface.SetDrawColor(255,255,255)
			surface.SetMaterial( markerMat )
			surface.DrawTexturedRect(pos.x, pos.y, 25, 38.5)
			draw.SimpleText( mark.title,"DermaDefault", pos.x+13, pos.y+42, Color( 255, 255, 255, 255 ),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
			draw.SimpleText( "Вызвает: "..mark.name,"DermaDefault", pos.x+13, pos.y+52, Color( 255, 255, 255, 255 ),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
			draw.SimpleText( "Адрес: "..mark.addr,"DermaDefault", pos.x+13, pos.y+62, Color( 255, 255, 255, 255 ),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
			draw.SimpleText( mark.desc or "","DermaDefault", pos.x+13, pos.y+72, Color( 255, 255, 255, 255 ),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
			draw.SimpleText( "Времени осталось: "..math.ceil(300-(CurTime()-mark.time)),"DermaDefault", pos.x+13, pos.y+82, Color( 255, 255, 255, 255 ),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
		end

	end
end
hook.Add("HUDPaint", "DrawMarkers", drawmarkers)
lastKeyPress = 0
hook.Add("KeyPress", "finishMarks", function(ply, key)
    if key ~= IN_USE or lastKeyPress > CurTime() - 0.2 then return end
    --print(" payphone debug pressed")
    lastKeyPress = CurTime()
    localplayer = localplayer or LocalPlayer()
    local hitman = localplayer:GetEyeTrace().Entity

    if not IsValid(hitman) or not hitman:IsPlayer() or localplayer:GetPos():DistToSqr(hitman:GetPos()) > minHitDistanceSqr then return end
    for i,mark in pairs(hudMarks) do
    	if mark.ply == hitman then
    		--print(" payphone debug sending")
    		net.Start("payphoneComm")
    		net.WriteString("finishMark")
    		net.WriteTable(mark)
    		net.SendToServer()
    		hudMarks[i] = nil
    		break
    	end
    end

    
end)