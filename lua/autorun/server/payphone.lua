--[[
Примерная схема работы платных вызовов:
1. На payphoneComm приходит запрос и вызвается CreatePayphoneBounty для нужной тимы
2. CreatePayphoneBounty проверяет всякие штуки и рассылает всем участникам нужной тимы запросы с таблицей с данными, и создает таймер на 20 секунд 
с отменой всех своих действий если ответ так и не пришел
3. У тех кто получил запросы выскакивают попапы с кнопками
4. Кнопки принятия в попапах посылают на payphoneComm команду acceptBounty и айдишнк из таблицы
5. payphoneComm вписывает первого принявшего оффер в поле assignTo и запускает таймер на проверку выполненности оффера раз в секунду(функция CheckPayPhoneBounty), и 
посылает команду создания метки нужному игроку
6. CheckPayPhoneBounty проверияет валидность таксы и игрока и расстояние между ними и если вышло время или игрок прибыл на точку завершает все и удаляет запись из таблицы

Выглядит слишком запутанно, но это лучшее что я смог придумать пока что
-Corpse
]]
sound.Add( {
	name = "roll_koleso",
	channel = CHAN_AUTO,
	volume = 1.0,
	level = 80,
	pitch = { 95, 110 },
	sound = "rashkinsk/phone/roll.wav"
} )

resource.AddWorkshop("1089796199")
--resource.AddFile( "materials/taxa.png" )
--resource.AddFile( "materials/koleso.png" )
--resource.AddFile( "materials/Waypoint.png" )
--resource.AddFile( "materials/zheleznaya_pizdyuchka.png" )
--resource.AddFile( "models/pantera_71/cphone_001/cphone_001.mdl" )
--resource.AddFile( "materials/models/pantera_71/cphone_001/truba.vmt" )
--resource.AddFile( "materials/models/pantera_71/cphone_001/telefon.vmt" )
--resource.AddFile( "materials/models/pantera_71/cphone_001/84d6328e.vmt" )


AddCSLuaFile("payphonederma.lua")
AddCSLuaFile("payphonedermapopup.lua")
util.AddNetworkString("payphoneComm")

CALL_PRICE = 300
payphoneBounties = payphoneBounties or {}
fireMarks = fireMarks or {}
policeMarks = policeMarks or {}



if !sql.TableExists("payphones") then
	print("Creating table")
	sql.Query([[CREATE TABLE `payphones` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`Address`	TEXT,
	`px`	FLOAT,
	`py`	FLOAT,
	`pz`	FLOAT,
	`r`	FLOAT,
	`y`	FLOAT,
	`p`	FLOAT
)]])
end

 --Место приема всех сетевых сообщений от клиента
net.Receive("payphoneComm",function (len,ply) 
	local command = net.ReadString()
	if command == "save" and (ply:GetUserGroup() == "creator" or ply:GetUserGroup() == "developer" or ply:GetUserGroup() == "manager" ) then
		local ent = net.ReadEntity()
		local addr = net.ReadString()
		ent:SetAddress(addr)
		if IsValid(ent) and ent.Save then ent:Save() end
		
	elseif command == "remove" and (ply:GetUserGroup() == "creator" or ply:GetUserGroup() == "developer" or ply:GetUserGroup() == "manager" ) then
		local ent = net.ReadEntity()
		if IsValid(ent) and ent.DBRemove then ent:DBRemove() end

	elseif command == "requestBounty" then
		local payphone = net.ReadEntity()
		local req_team = net.ReadInt(32)
		CreatePayphoneBounty(payphone,ply,req_team)

	elseif command == "acceptBounty" then
		local id = net.ReadInt(32)
		if not payphoneBounties[id] or payphoneBounties[id].assignTo then
			DarkRP.notify(ply,NOTIFY_GENERIC,10,"Вызов уже принят, вы опоздали.")
			return
		end
		payphoneBounties[id].assignTo = ply
		DarkRP.notify(payphoneBounties[id].caller,NOTIFY_GENERIC,10,"Ваш вызов принят, ожидайте возле таксофона")
		timer.Create("BountyCheckTimer"..id,1,0,function() CheckPayPhoneBounty(id) end)
		net.Start("payphoneComm")
		net.WriteString("general_mark")
		net.WriteTable(payphoneBounties[id])
		net.Send(ply)

	elseif command == "fire" then
		if #team.GetPlayers(TEAM_FIRE) == 0 then
			DarkRP.notify(ply,NOTIFY_GENERIC,10,"В данный момент все линии заняты, перезвоните позже.")
			return 
		end 
		for k,v in pairs(fireMarks) do
			if v.ply == ply then
				DarkRP.notify(ply,NOTIFY_GENERIC,10,"Слишком много вызовов с вашего номера, перезвоните позже")
				return
			end
		end
		local time = CurTime()
		fireMarks[time] = {}
		fireMarks[time]['phone'] 	= net.ReadEntity()
		fireMarks[time]['desc'] 	= net.ReadString()
		fireMarks[time]['name'] 	= ply:Nick()
		fireMarks[time]['ply']		= ply 
		fireMarks[time]['time'] 	= time --Это чтобы клиенты его нормально получали

		local rf = RecipientFilter()
		rf:AddRecipientsByTeam(TEAM_FIRE)
		net.Start("payphoneComm")
		net.WriteString("fire_mark")
		net.WriteTable(fireMarks[time])
		net.Send(rf)
		DarkRP.notify(ply,NOTIFY_GENERIC,10,"Вызов принят, ожидайте у таксофона")
		timer.Simple(300,function() fireMarks[time] = nil end)
	
	elseif command == "police" then
		if #team.GetPlayers(TEAM_CHIEF) + #team.GetPlayers(TEAM_DEZ) + #team.GetPlayers(TEAM_POLICE) + #team.GetPlayers(TEAM_GAI) + #team.GetPlayers(TEAM_BEAR) + #team.GetPlayers(TEAM_OMON)  + #team.GetPlayers(TEAM_MAYOR) == 0 then
			DarkRP.notify(ply,NOTIFY_GENERIC,10,"В данный момент все линии заняты, перезвоните позже.")
			return 
		end 
		for k,v in pairs(policeMarks) do
			if v.ply == ply then
				DarkRP.notify(ply,NOTIFY_GENERIC,10,"Слишком много вызовов с вашего номера, перезвоните позже")
				return
			end
		end
		local time = math.floor(CurTime())
		policeMarks[time] = {}
		policeMarks[time]['t'] = "police"
		policeMarks[time]['phone'] 	= net.ReadEntity()
		policeMarks[time]['desc'] 	= net.ReadString()
		policeMarks[time]['name'] 	= ply:Nick()
		policeMarks[time]['ply']		= ply 
		policeMarks[time]['time'] 	= time --Это чтобы клиенты его нормально получали

		local rf = RecipientFilter()
		for k,v in pairs(GAMEMODE.CivilProtection) do
			rf:AddRecipientsByTeam(k)
		end
		net.Start("payphoneComm")
		net.WriteString("police_mark")
		net.WriteTable(policeMarks[time])
		net.Send(rf)
		DarkRP.notify(ply,NOTIFY_GENERIC,10,"Вызов принят, ожидайте у таксофона")

	elseif command == "req_sound" then
		--print("heh")
		local ent = net.ReadEntity()
		ent:EmitSound( "roll_koleso" )

	elseif command == "acceptConfirm" then
		local id = net.ReadInt(32)
		b = payphoneBounties[id]
		if not b then return end
		if b.caller != ply then return end

		b.assignTo:addMoney(CALL_PRICE)
		DarkRP.notify(b.assignTo,NOTIFY_GENERIC,10,"Деньги за вызов выплачены")
		RemovePayphoneBounty(id)
		net.Start("payphoneComm")
		net.WriteString("delete_mark")
		net.WriteInt(id,32)
		net.Send(b.assignTo)
	elseif command == "finishMark" then

		--print("debug finish")
		data = net.ReadTable()
		--PrintTable(data)

		--print("debug finish 2")
		if data.t == "pay" then
			net.Start("payphoneComm")
			net.WriteString("delete_mark")
			net.WriteInt(data.id,32)
			net.Send(data.assignTo or ply)
			--print("debug finish 3")
			payphoneBounties[data.id].assignTo:addMoney(CALL_PRICE)
			DarkRP.notify(data.assignTo,NOTIFY_GENERIC,10,"Деньги за вызов выплачены")
			RemovePayphoneBounty(data.id)
			net.Start("payphoneComm")
			net.WriteString("delete_mark")
			net.WriteInt(data.id,32)
			net.Send(data.assignTo)
		end
		if data.t == "police" then
			policeMarks[data.time] = nil
		end
		

	end
 
end)	



--Пересылка существующих точек новым юзерам
hook.Add( "OnPlayerChangedTeam","sendLandMarks",function(ply,old,new)
	net.Start("payphoneComm")
	net.WriteString("delall")
	net.Send(ply)
	if new == TEAM_FIRE then
		for k,v in pairs(fireMarks) do
			net.Start("payphoneComm")
			net.WriteString("fire_mark")
			net.WriteTable(v)
			net.Send(ply)
		end
	elseif GAMEMODE.CivilProtection[new] then
		for k,v in pairs(fireMarks) do
			net.Start("payphoneComm")
			net.WriteString("police_mark")
			net.WriteTable(v)
			net.Send(ply)
		end
	end


end )



--Спавн такс при старте сервера
hook.Add( "InitPostEntity", "payphonesspawn", function()
	local data = sql.Query("SELECT * FROM payphones")
	if data then
		for k,v in pairs(data) do
			local ent = ents.Create("payphone")
			ent:SetPos(Vector(v.px,v.py,v.pz))
			ent:SetAngles(Angle(v.p,v.y,v.r))
			ent:Spawn()
			ent.saved = true
			ent.id = v.id
			ent:SetAddress(v.Address)
		end
	end
end )

--Функции платнхы вызовов
function CreatePayphoneBounty(payphone,caller,req_team)
	if #team.GetPlayers(req_team) == 0 then
		DarkRP.notify(caller,NOTIFY_GENERIC,10,"В данный момент все линии заняты, перезвоните позже")
		return
	end
	if not caller:canAfford(CALL_PRICE) then 
		DarkRP.notify(caller,NOTIFY_GENERIC,10,"На вашем счету недостаточно денег")
		return 
	end
	for k,v in pairs(payphoneBounties) do
		if caller == v.caller then
			DarkRP.notify(caller,NOTIFY_GENERIC,10,"Слишком много вызовов с вашего номера, перезвоните позже")
			return 
		end
	end
	--print("DEBUG money Before ",caller.DarkRPVars.money)
	DarkRP.notify(caller,NOTIFY_GENERIC,10,"Звонок принят, на вашем счету удержано 300 рублей")
	caller:addMoney(-CALL_PRICE)
	--print("DEBUG money After ",caller.DarkRPVars.money)

	local bounty = {}
	bounty.t = "pay"
	bounty.pos = payphone:GetPos()
	bounty.caller = caller
	bounty.name = caller:Nick()
	bounty.startTime = CurTime()
	bounty.assignTo = nil
	bounty.street = payphone:GetAddress()
	bounty.id = #payphoneBounties+1
	bounty.stage = 1 --Первая стадия - необходимо дойти до таксофона

	payphoneBounties[#payphoneBounties+1] = bounty
	net.Start("payphoneComm")
	net.WriteString("offerBounty")
	net.WriteTable(bounty)
	net.Send(team.GetPlayers(req_team))

	timer.Simple(20,function()
		if bounty.assignTo == nil then
			DarkRP.notify(caller,NOTIFY_GENERIC,10,"В данный момент все линии заняты, перезвоните позже")
			caller:addMoney(CALL_PRICE)
			RemovePayphoneBounty(bounty.id)
		end
	end)

end

function RemovePayphoneBounty(id)

	timer.Remove("BountyCheckTimer"..id)
	payphoneBounties[id] = nil
	
end

function CheckPayPhoneBounty(id)
	
	local b = payphoneBounties[id]
	if !IsValid(b.assignTo) then
		DarkRP.notify(b.caller,NOTIFY_GENERIC,10,"Сделка сорвалась, попробуйте еще раз")
		b.caller:addMoney(CALL_PRICE)
		RemovePayphoneBounty(b.id)
		return 
	end
	if CurTime() - b.startTime > 300 then
		DarkRP.notify(b.caller,NOTIFY_GENERIC,10,"Рабочий не успел прибыть на вызов, вам выплачена компенсация")

		b.caller:addMoney(CALL_PRICE)
		RemovePayphoneBounty(b.id)
		return
	end
	
	if b.stage == 1 then
		if b.assignTo:GetPos():DistToSqr(b.pos) < 30000 then
			DarkRP.notify(b.caller,NOTIFY_GENERIC,10,"Рабочий прибыл на место вызова")
			DarkRP.notify(b.assignTo,NOTIFY_GENERIC,10,"Вы прибыли, нажмите Е на вызывающем чтобы получить деньги")
			b.stage = 2
			b.caller.oldUse = b.caller.Use
			--print(b.caller.Use)
			b.caller.Use = function(self,activator, caller)
				if activator != b.assignTo then return end
				--print("Player used")
				net.Start("payphoneComm")
				net.WriteString("offerConfirm")
				net.WriteInt(b.id,32)
				net.Send(b.caller)
				b.caller.Use = b.caller.oldUse
			end
			--print(b.caller.Use)
		elseif b.stage == 2 then
			--b.assignTo:addMoney(CALL_PRICE)
			--DarkRP.notify(b.assignTo,NOTIFY_GENERIC,10,"Деньги за вызов выплачены")
		end
	end
end