
PANEL = {}
--Дерма которая выскакивает при нажатии на колесо, изначально сосотоит из сетки кнопок
--после вызова PANEL:Proceed превращается в форму конкретного вызова

function PANEL:Init()
	self:SetTitle("Таксофон")
	self:SetSize(235,550)
	self:SetSizable(false)
	self:SetDeleteOnClose(true)

	self.btnMaxim:SetVisible(true)
    self.btnMinim:SetVisible(false)
	self.Scroll = vgui.Create( "DScrollPanel", self )
	self.Scroll:SetSize(230,600)
	self.Scroll:SetPos(10,30)

	self.List = vgui.Create( "DIconLayout", self.Scroll )
	self.List:SetSize( 240, 600 )
	self.List:SetPos( 0, 0 )
	self.List:SetSpaceY( 5 ) 
	self.List:SetSpaceX( 15 ) 


	
	self:ScreenCenter() 
    self:MakePopup()
end
--Фикс кривого позиционирования DFrame
function PANEL:ScreenCenter()
	self:Center()
	local myx,myy = self:GetPos()
	local px,py = self:GetParent():GetPos()
	self:SetPos(myx+px,myy+py)

	end
function PANEL:AddBtn(label,key)
	btn = self.List:Add("DButton")
	btn:SetSize(100,100)
	btn:SetText(label)
	--btn:SetWrap(true)
	btn:SetTextInset(10,0)
	btn:SetContentAlignment(5)
	btn.DoClick = function() self:Proceed(key) end
	return btn
end
function PANEL:AddSpacer(text)
	local text = text or ""
	lbl = self.List:Add("DLabel")
	lbl:SetSize(100,35)
	lbl:SetText(text)
	lbl:SetWrap(true)
	return lbl
end
function PANEL:AddCloseBtn()
	btn = self.List:Add("DButton")
	btn:SetSize(215,50)
	btn:SetText("Не буду никуда звонить")
	btn.DoClick = function() self:GetParent():Close() end
end
function PANEL:Revert()
	for k,v in pairs(self:GetChildren()) do
		v:SetVisible(false)
	end
	self.lblTitle:SetVisible(true)
	self.btnClose:SetVisible(true)
	self:SetSize(235,550)
	self.Scroll:SetVisible(true)
	self:ScreenCenter() 

end
function PANEL:Proceed(key)
	local data = callsData[key]

	for k,v in pairs(self:GetChildren()) do
		v:SetVisible(false)
	end
	self.lblTitle:SetVisible(true)
	self.btnClose:SetVisible(true)

	self:SetSize(400,300)
	self:ScreenCenter() 
	self:SetTitle(data["title"])

	self.lbl = vgui.Create("DLabel",self)
    self.lbl:SetPos(15,30)
    self.lbl:SetSize(370,80)
    self.lbl:SetText(data["label"])
    self.lbl:SetWrap(true)

    if data["entry"] then
    	self.elbl = vgui.Create("DLabel",self)
    	self.elbl:SetPos(10,110)
    	self.elbl:SetText(data["entry"])
    	self.elbl:SizeToContents()
    	self.entry = vgui.Create("DTextEntry",self)
    	self.entry:SetSize(290,15)
    	self.entry:SetPos(10,125)
    		
    end

    if data["DoClick"] then
	    self.call = vgui.Create("DButton",self)
	    self.call:SetText("Вызвать")
	    self.call:SetSize(70,30)
	    self.call:SetPos(10,145)
	    self.call.DoClick = data["DoClick"]
	    
	end
	self.no = vgui.Create("DButton",self)
    self.no:SetText("Передумал")
    self.no:SetSize(70,30)
    self.no:SetPos(90,145)
    self.no.DoClick = function() self:Revert() end
	if data['bText'] then
		self.call:SetText(data["bText"])
	end
end

	
vgui.Register("PayphoneDermaPopup",PANEL,"DFrame")

function getRequestFuncFor(req_team)
	print(req_team)
	return function(self) 
			self = self:GetParent() 
			net.Start("payphoneComm")
			net.WriteString("requestBounty")
			net.WriteEntity(self:GetParent().ent)
			print(req_team)
			net.WriteInt(req_team,32)
			net.SendToServer()
			self:GetParent():Close()
		end
end
 
--Данные для формирования формы вызова
--поля entry, DoClick b btext  опциональны 
callsData = {}

timer.Simple(10, function()
callsData = {
	["fire"] = {	
		["title"] 	= "Пожарная служба",
		["label"] 	= "Вызвать пожарную службу?",
		["entry"] 	= "Опишите ситуацию",
		["DoClick"]	= function(self) 
						self = self:GetParent() 
						net.Start("payphoneComm")
						net.WriteString("fire")
						net.WriteEntity(self:GetParent().ent)
						net.WriteString(string.sub(string.Trim(self.entry:GetText() or ""),0,80))
						net.SendToServer()
						self:GetParent():Close()

		 			end
	},
	["med"] = {	
		["title"] 	= "Скорая помощь",
		["label"] 	= "Все больницы рашкинска недоступны в связи с недостатком финансирования. Воспользуйтесь услугой липосакции в ближайшем диетологическом отделении.",
	},
	["police"] = {	
		["title"] 	= "Полиция",
		["label"] 	= "Вызвать полицию?",
		["entry"] 	= "Опишите ситуацию",
		["DoClick"]	= function(self) 
						self = self:GetParent() 
						net.Start("payphoneComm")
						net.WriteString("police")
						net.WriteEntity(self:GetParent().ent)
						net.WriteString(string.sub(string.Trim(self.entry:GetText() or ""),0,80))
						net.SendToServer()
						self:GetParent():Close()

		 			end
	},
	["gas"] = {	
		["title"] 	= "Газовая служба",
		["label"] 	= "Служба закрыта в связи с недостатком финансирования.",
	},
	["diet"] = {	
		["title"] 	= "Диетологическая клиника",
		["label"] 	= "Здравствуйте! Вы позвонили в лучшую диетологическую клинику в г. Рашкинск! Вас мучают мигрени? Отстрелили половину лица? Не проходит понос? Вызовите диетолога! С помощью липосакции наши сотрудники легко поставят Вас на ноги! С нами можете быть здоровыми и худыми! (Вызов платный)",
		["DoClick"]	= getRequestFuncFor(TEAM_MEDIC),
		["btext"] = "Вызвать(300р)"
	},
	["taxi"] = {	
		["title"] 	= "Такси",
		["label"] 	= "Ээээ, Вася! Заблудился? Я тебя выручу, братишка, ты только свистни, родной! (Вызов платный)",
		["DoClick"]	= getRequestFuncFor(TEAM_TAXI),
		["btext"] = "Вызвать(300р)"
	},
	["cook"] = {	
		["title"] 	= "Повар",
		["label"] 	= "Приветствуем Вас. Вы голодны, если нет, то положите трубку, если да, то Вы можете заказать доставку еды на дом! (Вызов платный)",
		["DoClick"]	= getRequestFuncFor(TEAM_COOK),
		["btext"] = "Вызвать(300р)"
	},
	["mech"] = {	
		["title"] 	= "Механик",
		["label"] 	= "Здравствуйте! Вы позвонили в единую службу помощи на дороге! Сломалась машина? Собака искусала колёса? Бандиты взорвали машину? Не беда! Вы обратились по адресу. Ближайший механик приедет к вам! (Вызов платный)",
		["DoClick"]	= getRequestFuncFor(TEAM_MECHANIC),			
		["btext"] = "Вызвать(300р)"
	},

}
end )

numberOfPopups = 0
PayphoneBountyOffers = {}
local PANEL = {}
function PANEL:Init()

	self:SetSize(200,180)
	self:SetPos(ScrW()-200,(190*numberOfPopups)+200)
	self.number = numberOfPopups
	PayphoneBountyOffers[#PayphoneBountyOffers+1] = self
	numberOfPopups = numberOfPopups + 1
	self:SetTitle("")

	self:SetSizable(false)
	self.btnClose:SetVisible(false)
	self.btnMaxim:SetVisible(false)
	self.btnMinim:SetVisible(false)
	local lblTitle = vgui.Create("DLabel",self)
	lblTitle:SetText("Поступил вызов")
	lblTitle:SetFont("Trebuchet24")
	lblTitle:SetTextColor(Color(240,240,240))
	lblTitle:SetPos(10,7)
	lblTitle:SizeToContents()
	self.Paint = function(pnl,w,h)

	    draw.RoundedBox(1,0,0,w,h,Color(20,20,20))
	    draw.RoundedBox(1,0,0,w,40,Color(204,52,38))
	    return true
	end

	self.label = vgui.Create("DLabel", self)
	self.label:SetText("")
	self.label:SetPos(20,45)
	self.label:SetSize(160,75)
	self.label:SetWrap(true)
	self.label:SetTextColor(Color(240,240,240))

	self.ybutton = vgui.Create("DButton",self)
	self.ybutton:SetSize(70,30)
	self.ybutton:SetText("Согласиться")
	self.ybutton:SetPos(30,140)
	self.ybutton:SetTextColor(Color(240,240,240))
	self.ybutton.DoClick = function()
	    net.Start("payphoneComm")
	    net.WriteString("acceptBounty")
	    net.WriteInt(self.id,32)
	    net.SendToServer()
	    self:Remove()
	end
	self.ybutton.Paint = function(pnl,w,h)
	    draw.RoundedBox(2,0,0,w,h,Color(0,0,0))
	    draw.RoundedBox(2,1,1,w-2,h-2,Color(138,197,57))
	end
	self.nbutton = vgui.Create("DButton",self)
	self.nbutton:SetSize(70,30)
	self.nbutton:SetText("Отказаться")
	self.nbutton:SetPos(110,140)
	self.nbutton:SetTextColor(Color(240,240,240))
	self.nbutton.DoClick = function() 
	    self:Remove()
	end
	self.nbutton.Paint = function(pnl,w,h)
	    draw.RoundedBox(2,0,0,w,h,Color(0,0,0))
	    draw.RoundedBox(2,1,1,w-2,h-2,Color(204,52,38))
	end
	timer.Simple(20, function() self:Remove() end ) 
	self:SetVisible(true)
	self:SetMouseInputEnabled(true)
end
function PANEL:OnRemove()
	numberOfPopups = numberOfPopups -1
	for k,v in pairs(PayphoneBountyOffers) do
		print(v,v.number,self.number)
		if IsValid(v) and (v.number > self.number) then
			x,y = v:GetPos()
			v:SetPos(x,y-170)
		end
	end
end

function PANEL:SetText(text)
	self.label:SetText(text)
end
vgui.Register("PayphoneBountyOfferPopup",PANEL,"DFrame")